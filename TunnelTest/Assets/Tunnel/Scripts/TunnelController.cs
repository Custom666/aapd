﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using Assets;
using System.Linq;
using UnityEngine;
using Zenject;

public class TunnelController : MonoBehaviour
{
    [Inject] private Tunnel<Data> _tunnel;

    [SerializeField] private GameObject _pointPrefab;
    [SerializeField] private float _size = 100;

    public void DisplayTunnel(DataType type)
    {
        if (transform.childCount > 0) transform.ClearChildren();

        // vezmu si kolekci podle DataType 
        var collection = _tunnel.RawData.First(col => col.Any(data => data.DataType == type)).ToArray();

        // vezmu si vzdalenost prvniho bodu abych pote mohl vsechny ostatni posunout podle teto
        var distance = collection[0].Vectors[0].CreateVector3() * _size;

        foreach (var point in collection)
        {
            // vytvorim novou instanci bodu
            var newPoint = Instantiate(_pointPrefab, transform);

            // nastavim pozici bodu ve scene
            var newPosition = point.Vectors[0].CreateVector3() * _size;

            // posunu bod abych ho mel na pocatku a nikoliv nekde ve vzduchu (kvuli nasobeni _size, aby sli pointy videt)
            newPoint.transform.position = newPosition - distance;
        }
    }
}
