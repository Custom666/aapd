﻿using UnityEngine;

namespace Assets
{
    public static class TransformExtension
    {
        public static void ClearChildren(this Transform transform)
        {
            foreach (Transform child in transform) Object.Destroy(child.gameObject);
        }
    }

    public static class Vector3DExtension
    {
        public static Vector3 CreateVector3(this AAPD_Data.Models.Vector3D vector) => new Vector3(vector.x, vector.y, vector.z);
    }
}
