﻿using AAPD_Data.Enums;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TunnelController _tunnelController;
    [SerializeField] private Dropdown _dropdown;
    
    public void CreateTunnelButtonClick()
    {
        _tunnelController.DisplayTunnel((DataType)_dropdown.value);
    }
}
