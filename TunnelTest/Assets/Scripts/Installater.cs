﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Assets
{
    public class Installater : MonoInstaller
    {
        private static readonly DataCollection<Data>[] _collections = new Dictionary<DataType, string>
        {
            { DataType.Forearm, @"Assets\Forearm.txt" },
            { DataType.Arm, @"Assets\Arm.txt" },
            { DataType.Head, @"Assets\Head.txt" },
            { DataType.Chest, @"Assets\Chest.txt" },
        }.Select(file => DataCollection<Data>.LoadData(file.Value, file.Key)).ToArray();
        
        public override void InstallBindings()
        {
            Container.Bind<Tunnel<Data>>().AsSingle().WithArguments(_collections);
        }
    }
}
