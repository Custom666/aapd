﻿namespace KDTree
{
    public class Node<T>
    {
        public float DistanceFromRoot { get; set; }

        public int Axis { get; set; }

        public Node<T> LeftChild { get; set; }

        public Node<T> RightChild { get; set; }

        public T[] Value { get; set; }

        public Node(int axis, T[] value, float distanceFromRoot)
        {
            Axis = axis;
            Value = value;
            DistanceFromRoot = distanceFromRoot;
        }

        public override string ToString() => $"[{ string.Join(";", Value) }]";
    }
}
