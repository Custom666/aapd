﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KDTree.Extensions
{
    public static class Extensions
    {
        public static T GetMedian<T>(this IEnumerable<T> values)
        {
            if (values.Count() == 1) return values.First();

            var sorted = values.OrderBy(x => x).ToList();

            return sorted[((sorted.Count() - 1) / 2)];
        }

        public static double Distance<T>(this IList<T> value, IList<T> other, Func<T, T, double> difference)
        {
            var result = 0d;

            for (var index = 0; index < value.Count; index++)
            {
                var diff = difference(other[index], value[index]);

                result += (diff * diff);
            }

            return Math.Sqrt(result);
        }

        public static float Map(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}
