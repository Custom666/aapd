﻿using KDTree.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KDTree
{
    public class KDTree<T> where T : IComparable<T>
    {
        private class ValuesWithDistance<T> where T : IComparable<T>
        {
            public T[] values;

            public float distanceFromBeginning;
        }

        private readonly Func<T, T, double> _difference;
        public readonly int Dimension;

        public Node<T> Root { get; private set; }

        public float TotalDistance { get; private set; }

        public KDTree(int dimension, Func<T, T, double> difference)
        {
            Dimension = dimension;
            _difference = difference;
        }

        public KDTree(int dimension, Func<T, T, double> difference, IList<T[]> values) : this(dimension, difference)
        {
            GenerateTree(values);
        }

        public void GenerateTree(IList<T[]> values)
        {
            List<ValuesWithDistance<T>> input = new List<ValuesWithDistance<T>>();

            for (int i = 0; i < values.Count; i++)
            {
                double distance = 0d;

                if (i - 1 >= 0) distance = input[i - 1].distanceFromBeginning + values[i - 1].Distance(values[i], _difference);


                input.Add(new ValuesWithDistance<T>
                {
                    values = values[i],
                    distanceFromBeginning = (float)distance
                });
            }

            TotalDistance = input[input.Count - 1].distanceFromBeginning;

            Root = generateTree(null, 0, input);
        }

        private Node<T> generateTree(Node<T> parentNode, int axis, IList<ValuesWithDistance<T>> values)
        {
            int currentAxis = axis % Dimension;

            T median = values.Select(x => x.values[currentAxis]).GetMedian();

            ValuesWithDistance<T> value = values.First(x => x.values[currentAxis].CompareTo(median) == 0);

            Node<T> node = new Node<T>(currentAxis, value.values, value.distanceFromBeginning);

            List<ValuesWithDistance<T>> lowerValues = values.Where(x => x.values[currentAxis].CompareTo(median) < 0).ToList();
            List<ValuesWithDistance<T>> biggerValues = values.Where(x => x.values[currentAxis].CompareTo(median) > 0).ToList();

            if (lowerValues.Count > 0) node.LeftChild = generateTree(node, axis + 1, lowerValues);
            if (biggerValues.Count > 0) node.RightChild = generateTree(node, axis + 1, biggerValues);

            return node;
        }

        public Node<T> Search(T[] value)
        {
            return search(Root, value, 0);
        }

        private Node<T> search(Node<T> node, T[] value, int axis)
        {
            if (node == null) return null;

            Node<T> nextBranch = null;
            Node<T> oppositeBranch = null;

            int currentAxis = axis % Dimension;

            // determine next branch to traverse (left or right)
            if (_difference(value[currentAxis], node.Value[currentAxis]) < 0)
            {
                nextBranch = node.LeftChild;
                oppositeBranch = node.RightChild;
            }
            else
            {
                nextBranch = node.RightChild;
                oppositeBranch = node.LeftChild;
            }

            // get nearest node
            Node<T> nearest = closestDistance(value, search(nextBranch, value, axis + 1), node);

            // check other branch if there is closer distance
            if (value.Distance(nearest.Value, _difference) >= Math.Abs(_difference(value[currentAxis], node.Value[currentAxis])))
            {
                nearest = closestDistance(value, search(oppositeBranch, value, axis + 1), nearest);
            }

            return nearest;
        }

        private Node<T> closestDistance(T[] value, Node<T> node1, Node<T> node2)
        {
            if (node1 == null) return node2;
            if (node2 == null) return node1;

            double distance1 = node1.Value.Distance(value, _difference);
            double distance2 = node2.Value.Distance(value, _difference);

            return distance1 < distance2 ? node1 : node2;
        }
    }
}
