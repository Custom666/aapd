﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Infrastructure;
using KDTree;
using KDTree.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace KDTreeTests
{
    [TestClass]
    public class KDTreeTest
    {
        private readonly Dictionary<float, float[]> _points;

        public KDTreeTest()
        {
            _points = new Dictionary<float, float[]>();

            var collections = new DataCollection<Data>[]
            {
                DataCollection<Data>.LoadData(@"Vive/All\Head.txt", AAPD_Data.Enums.DataType.Head),
                DataCollection<Data>.LoadData(@"Vive/All/Chest.txt", AAPD_Data.Enums.DataType.Chest),
                DataCollection<Data>.LoadData(@"Vive/All/Forearm.txt", AAPD_Data.Enums.DataType.Forearm),
                DataCollection<Data>.LoadData(@"Vive/All/Arm.txt", AAPD_Data.Enums.DataType.Arm)
            };

            var min = collections.Min(x => x.Items.Count);

            for (var i = 0; i < min; i++)
            {
                var time = 0f;
                var destination = new float[collections.Length * 6];

                for (var index = 0; index < collections.Length; index++)
                {
                    time += collections[index].Items[i].Time;

                    var source = collections[index].Items[i].ToArray(Settings.Coefficients[collections[index].Items[i].DataType]);

                    Array.Copy(source, 0, destination, index * source.Length, source.Length);
                }

                time /= collections.Length;

                if (!_points.ContainsKey(time)) _points.Add(time, destination);
            }
        }

        /// <summary>
        ///            [30,40]
        ///    [10,12]         [35,45]
        ///       [5,25]    [50,30] [70,70]
        /// </summary>
        [TestMethod]
        public void ConstructTreeFromConstructor()
        {
            var points = new List<float[]>
             {
               new float[] { 30, 40 },
               new float[] { 5, 25 } ,
               new float[] { 10, 12 },
               new float[] { 70, 70 },
               new float[] { 50, 30 },
               new float[] { 35, 45 }
             };

            var tree = new KDTree<float>(points[0].Length, (x, y) => x - y, points);

            // root 
            Assert.AreEqual(points[0], tree.Root.Value);

            // left tree from root
            Assert.AreEqual(points[2], tree.Root.LeftChild.Value);
            Assert.AreEqual(points[1], tree.Root.LeftChild.RightChild.Value);

            // right tree from root
            Assert.AreEqual(points[5], tree.Root.RightChild.Value);
            Assert.AreEqual(points[4], tree.Root.RightChild.LeftChild.Value);
            Assert.AreEqual(points[3], tree.Root.RightChild.RightChild.Value);
        }

        [TestMethod]
        public void SearchNearestNeighbour()
        {
            Stopwatch sw = null;
            Node<float> result = null;
            var bruteForceTimes = new List<double>();
            var nearestNeighbourTimes = new List<double>();

            var last = 0f;
            var untilMidnight = 0f;

            var points = new List<float[]>();

            new List<DataCollection<Data>>()
            {
                DataCollection<Data>.LoadData(@"Vive/All/Chest.txt", AAPD_Data.Enums.DataType.Chest),
                DataCollection<Data>.LoadData(@"Vive/All/Forearm.txt", AAPD_Data.Enums.DataType.Forearm),
                DataCollection<Data>.LoadData(@"Vive/All\Head.txt", AAPD_Data.Enums.DataType.Head),
                DataCollection<Data>.LoadData(@"Vive/All/Arm.txt", AAPD_Data.Enums.DataType.Arm)
            }.ForEach(x =>
            {
                foreach (var item in x.Items) points.Add(item.ToArray(Settings.Coefficients[AAPD_Data.Enums.DataType.Arm]));
            });

            //var points = _points;

            var tree = new KDTree<float>(3, (x, y) => x - y, points);

            var pointToSearch = new float[] { -0.2783f, 1.2431f, -0.0014f, 12.7738f, 10.9967f, 3.3575f }; // middle
                              //new float[] { -0.2765f, 1.2755f, 0.01944f, -7.5555f, 10.2081f, 0.1780f }; // last
                              //new float[] { -0.2974232f, 1.254929f, -0.01909679f, 1.057941f, 9.930037f, 4.509134f,
                              //              -0.3057893f,0.9150553f,0.02510844f, -72.23724f, -168.9551f, 5.052947f,
                              //               0.03516817f,0.6555611f,0.1005663f, 34.29169f, 15.07501f, 125.9417f,
                              //              -0.08502639f,0.8716848f,-0.06386478f, 67.12828f, 77.11886f, 37.09531f};

            for (int index = 0; index < 100; index++)
            {
                sw = Stopwatch.StartNew();  

                result = tree.Search(pointToSearch);

                nearestNeighbourTimes.Add(((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000);
            }

            // brute force
            var bestDistance = double.MaxValue;
            float[] expectedResult = null;

            for (int index = 0; index < 100; index++)
            {
                sw = Stopwatch.StartNew();

                foreach (var point in points)
                {
                    var distance = point.Distance(pointToSearch, (x, y) => x - y);

                    if (distance < bestDistance)
                    {
                        bestDistance = distance;

                        expectedResult = point;
                    }
                }

                bruteForceTimes.Add(((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000);
            }

            var bruteForceMedian = bruteForceTimes.ToArray().GetMedian();
            var nearestNeighbourMedian = nearestNeighbourTimes.ToArray().GetMedian();

            Assert.AreEqual(expectedResult, result.Value);
            Assert.IsTrue(Math.Log(bruteForceMedian) >= nearestNeighbourMedian, $"Actual value [{ nearestNeighbourMedian }] should be smaller than expected value [{ Math.Log(bruteForceMedian) }]");
        }
    }
}
