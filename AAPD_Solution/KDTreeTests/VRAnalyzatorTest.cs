﻿using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using AAPD_Data.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using AAPD_Data.Extensions;
using AAPD_Core.Infrastructure;
using AAPD_Core.Processors;

namespace KDTreeTests
{
    [TestClass]
    public class VRAnalyzatorTest
    {
        private const int Dimension = 24;

        private static IDictionary<DataType, string> _correctFiles = new Dictionary<DataType, string>
        {
            { DataType.Head,    @"Vive\Correct\ThirdCorrectMoveUp\Head.txt" },
            { DataType.Chest,   @"Vive\Correct\ThirdCorrectMoveUp\Chest.txt" },
            { DataType.Forearm, @"Vive\Correct\ThirdCorrectMoveUp\Forearm.txt" },
            { DataType.Arm,     @"Vive\Correct\ThirdCorrectMoveUp\Arm.txt" },
        };

        private DataCollection<Data>[] _correctCollection = _correctFiles.Select(file => DataCollection<Data>.LoadData(file.Value, file.Key)).ToArray();

        [TestMethod]
        public void TestCorrectInput()
        {
            var tunnel = new Tunnel<Data>(_correctCollection, new DataProcessor(Dimension));

            for (int i = 0; i < _correctCollection[0].Items.Count; i++)
            {
                tunnel.Processor.Input(_correctCollection[0].Items[i]);
                tunnel.Processor.Input(_correctCollection[1].Items[i]);
                tunnel.Processor.Input(_correctCollection[2].Items[i]);
                tunnel.Processor.Input(_correctCollection[3].Items[i]);

                tunnel.Processor.Process(out var p, out var q);

                var expectedPhase = ((float)i).Map(0, _correctCollection[0].Items.Count, 0f, 1f);

                Assert.AreEqual(expectedPhase, p, 0.015f);
                Assert.AreEqual(1f, q);
            }
        }

        [TestMethod]
        public void TestFirstCorrectInput()
        {
            var tunnel = new Tunnel<Data>(_correctCollection, new DataProcessor(Dimension));

            foreach (var collection in _correctCollection) tunnel.Processor.Input(collection.Items[0]);

            tunnel.Processor.Process(out var p, out var q);

            Assert.AreEqual(0f, p);
            Assert.AreEqual(1f, q);
        }

        [TestMethod]
        public void TestLastCorrectInput()
        {
            var tunnel = new Tunnel<Data>(_correctCollection, new DataProcessor(Dimension));

            foreach (var collection in _correctCollection) tunnel.Processor.Input(collection.Items.Last());

            tunnel.Processor.Process(out var p, out var q);

            Assert.AreEqual(1f, p);
            Assert.AreEqual(1f, q);
        }

        [TestMethod]
        public void TunnelTransformTest()
        {
            var tunnel = new Tunnel<Data>(_correctCollection, new DataProcessor(Dimension));

            var destinationData = new Data
            {
                DataType = DataType.Chest,
                Vectors = new Vector3D[] { Vector3D.X }
            };

            tunnel.TransformTo(destinationData);

            var actual = tunnel.RawData.First(collection => collection.Any(data => data.DataType == destinationData.DataType)).First().Vectors[0];

            Assert.AreEqual(destinationData.Vectors[0], actual);
        }

        [TestMethod]
        public void ProcessorsTest()
        {
            var tunnel = new Tunnel<Data>(_correctCollection)
            {
                Processor = new FileDataProcessor(@"Vive\Marked\MarkedDataAll.txt")
            };

            tunnel.Processor.Process(out var phase, out var quality);

            Assert.AreEqual(.5f, phase, .5f); // result value is not important
            Assert.AreEqual(.5f, quality, .5f); // result value is not important

            tunnel.Processor = new DataProcessor(Dimension);

            foreach (var collection in _correctCollection) tunnel.Processor.Input(collection.Items.Last());

            tunnel.Processor.Process(out phase, out quality);

            Assert.AreEqual(.5f, phase, .5f); // result value is not important
            Assert.AreEqual(.5f, quality, .5f); // result value is not important
        }
    }
}
