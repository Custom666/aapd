﻿using System;

namespace AAPD_Data.Models
{
    /// <summary>
    /// Three-dimensional comparable vector.
    /// <remarks>
    /// <para>This struct can add, subtract or multiply.</para>
    /// <para>Static methods for obtaining axis vectors are available.</para>
    /// </remarks>
    /// </summary>
    public struct Vector3D : IComparable<Vector3D>
    {
        /// <value>
        /// X-coordinate.
        /// </value>
        public float x;

        /// <value>
        /// Y-coordinate.
        /// </value>
        public float y;

        /// <value>
        /// Z-coordinate.
        /// </value>
        public float z;

        /// <value>
        /// New empty vector.
        /// </value>
        public static Vector3D Empty => new Vector3D(0f, 0f, 0f);

        /// <value>
        /// A new vector describing the x-coordinate axis.
        /// </value>
        public static Vector3D X => new Vector3D(1f, 0f, 0f);

        /// <value>
        /// A new vector describing the y-coordinate axis.
        /// </value>
        public static Vector3D Y => new Vector3D(0f, 1f, 0f);

        /// <value>
        /// A new vector describing the z-coordinate axis.
        /// </value>
        public static Vector3D Z => new Vector3D(0f, 0f, 1f);

        /// <summary>
        /// Creates a new vector based on the passed values.
        /// </summary>
        /// <param name="x">X-coordinate.</param>
        /// <param name="y">Y-coordinate.</param>
        /// <param name="z">Z-coordinate.</param>
        public Vector3D(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Adding two vectors. This operation create new vector.
        /// </summary>
        /// <param name="v1">First vector.</param>
        /// <param name="v2">Second vector.</param>
        /// <returns></returns>
        public static Vector3D operator +(Vector3D v1, Vector3D v2) => new Vector3D(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);

        /// <summary>
        /// Subtraction of two vectors. This operation create new vector.
        /// </summary>
        /// <param name="v1">Vector from which it is deducted.</param>
        /// <param name="v2">Vector that is subtracted.</param>
        /// <returns></returns>
        public static Vector3D operator -(Vector3D v1, Vector3D v2) => new Vector3D(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);

        /// <summary>
        /// Vector multiplication by value. This operation create new vector.
        /// </summary>
        /// <param name="vector">Vector.</param>
        /// <param name="value">Multiplication value.</param>
        /// <returns></returns>
        public static Vector3D operator *(Vector3D vector, float value) => new Vector3D(vector.x * value, vector.y * value, vector.z * value);

        /// <summary>
        /// String interpretation of vector.
        /// </summary>
        /// <returns>String in following format <c>x,y,z</c>.</returns>
        public override string ToString()
        {
            return $"{x},{y},{z}";
        }

        /// <summary>
        /// Compare this vector with another vector <paramref name="other"/>. 
        /// The comparison is based on the differences between the individual vector components.
        /// </summary>
        /// <param name="other">Compared vector.</param>
        /// <returns>Return <c>0</c> if vectors has same values. Otherwise return <c>-1</c>.</returns>
        public int CompareTo(Vector3D other)
        {
            var xDiff = other.x - x;
            var yDiff = other.y - y;
            var zDiff = other.z - z;

            return xDiff == 0 && yDiff == 0 && zDiff == 0 ? 0 : -1;
        }
    }

   
}
