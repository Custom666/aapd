﻿using AAPD_Data.Extensions;
using System;

namespace AAPD_Data.Models
{
    /// <summary>
    /// Matrix 3x3.
    /// </summary>
    public class Matrix3x3
    {
        /// <summary>
        /// Raw matrix data.
        /// </summary>
        public float[][] Matrix { get; private set; }

        /// <summary>
        /// Create new empty matrix.
        /// </summary>
        public Matrix3x3()
        {
            Matrix = new float[3][];

            for (int i = 0; i < 3; i++) Matrix[i] = new float[3];
        }

        /// <summary>
        /// Create new matrix based on given data <paramref name="matrix"/>.
        /// </summary>
        /// <param name="matrix">Raw data.</param>
        public Matrix3x3(float[][] matrix)
        {
            Matrix = matrix;
        }

        /// <summary>
        /// Creates a rotation matrix by a given rotation angle about a given rotation axis.
        /// </summary>
        /// <param name="axis">Rotation axis.</param>
        /// <param name="angle">Rotation angle.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix3x3 CreateRotation(Vector3D axis, float angle)
        {
            var angleInRadians = angle.ToRadians();
            var cosAngle = (float)Math.Cos(angleInRadians);
            var sinAngle = (float)Math.Sin(angleInRadians);

            if (axis.Equals(Vector3D.X))
            {
                return new Matrix3x3(new float[][]
                {
                    new float[] { 1,    0,          0           },
                    new float[] { 0,    cosAngle,   -sinAngle   },
                    new float[] { 0,    sinAngle,   cosAngle    }
                });
            }
            else if (axis.Equals(Vector3D.Y))
            {
                return new Matrix3x3(new float[][]
                {
                    new float[] { cosAngle,     0,  sinAngle    },
                    new float[] { 0,            1,  0           },
                    new float[] { -sinAngle,    0,  cosAngle    }
                });
            }
            else if (axis.Equals(Vector3D.Z))
            {
                return new Matrix3x3(new float[][]
              {
                    new float[] { cosAngle,     -sinAngle,  0   },
                    new float[] { sinAngle,     cosAngle,   0   },
                    new float[] { 0,            0,          1   }
              });
            }

            throw new FormatException($"Bad rotation axis! Value was { axis }");
        }

        /// <summary>
        /// Create new matrix from vector <paramref name="rotation"/> that represents euler angles
        /// in given <paramref name="order"/>.
        /// </summary>
        /// <param name="rotation">Euler angles vector <see cref="Vector3D"/>.</param>
        /// <param name="order">Euler angles order. <example><c>"ZXY"</c></example></param>
        /// <returns>Mattrix made of euler angles.</returns>
        /// <exception cref="FormatException">Throw new exception when given <paramref name="order"/> is not implemented.</exception>
        public static Matrix3x3 CreateFromEulerAngles(Vector3D rotation, string order)
        {
            var cosX = (float)Math.Cos(rotation.x.ToRadians());
            var cosY = (float)Math.Cos(rotation.y.ToRadians());
            var cosZ = (float)Math.Cos(rotation.z.ToRadians());
            var sinX = (float)Math.Sin(rotation.x.ToRadians());
            var sinY = (float)Math.Sin(rotation.y.ToRadians());
            var sinZ = (float)Math.Sin(rotation.z.ToRadians());

            /*switch (order)
            {*/
                //case "ZYX":
                //    return new Matrix3x3(new float[][]
                //    {
                //        new float[] { cosZ * cosY,                          -cosY * sinZ,                       sinY },
                //        new float[] { cosX * sinZ + cosZ * sinX * sinY,     cosZ * cosX - sinZ * sinY * sinX,   -cosY * sinX },
                //        new float[] { sinX * sinZ - cosX * cosZ * sinY,     cosZ * sinX + cosX * sinY * sinZ,   cosY * cosX }
                //    });

                //case "XYZ":
                //    return new Matrix3x3(new float[][]
                //    {
                //        new float[] { cosZ * cosY,  cosZ * sinY * sinX - cosX * sinZ,   sinZ * sinX + cosZ * cosX * sinY},
                //        new float[] { cosY * sinZ,  cosZ * cosX + sinZ * sinY * sinX,   cosX * sinZ * sinY - cosZ * sinX},
                //        new float[] { -sinY,        cosY * sinX,                        cosY * cosX}
                //    });

                //case "YXZ":
                //    return new Matrix3x3(new float[][]
                //    {
                //        new float[] { cosZ * cosY - sinZ * sinX * sinY,     -cosX * sinZ,   cosZ * sinY + cosY * sinZ * sinX},
                //        new float[] { cosY * sinZ + cosZ * sinX * sinY,     cosZ * cosX,    sinZ * sinY - cosZ * sinY * sinX},
                //        new float[] { -cosX * sinY,                         sinX,           cosX * cosY}
                //    });
                //case "ZXY":
                    return new Matrix3x3(new float[][]
                    {
                        new float[] { cosY * cosZ + sinZ * sinX * sinY,     cosZ * sinY * sinX - cosY * sinZ,   cosX * sinY},
                        new float[] { cosX * sinZ,                          cosX * cosZ,                        -sinX},
                        new float[] { cosY * sinX * sinZ - cosZ * sinY,     cosY * cosZ * sinX + sinY * sinZ,   cosX * cosY}
                    });

                //default: throw new FormatException($"Given order { order } is not implemented!");
            //}
        }
    }
}
