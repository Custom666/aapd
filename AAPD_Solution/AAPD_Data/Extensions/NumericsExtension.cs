﻿using AAPD_Data.Models;
using System;
using System.Collections.Generic;

namespace AAPD_Data.Extensions
{
    /// <summary>
    /// Extension of <see cref="Vector3D"/> functionality.
    /// </summary>
    public static class Vector3DExtension
    {
        /// <summary>
        /// Scalar product of two vectors <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="original">First vector.</param>
        /// <param name="vector">Second vector.</param>
        /// <returns></returns>
        public static float DotProduct(this Vector3D original, Vector3D vector)
        {
            return vector.x * original.x + vector.y * original.y + vector.z * original.z;
        }

        /// <summary>
        /// Magnitude of vector <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="vector">Input vector.</param>
        /// <returns></returns>
        public static float SqrtMagnitude(this Vector3D vector)
        {
            return (float)Math.Sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
        }
    }

    /// <summary>
    /// Extension of <see cref="Matrix3x3"/> functionality.
    /// </summary>
    public static class Matrix3x3Extension
    {
        /// <summary>
        /// Multiplication of two matrix <see cref="Matrix3x3"/>. 
        /// </summary>
        /// <param name="original">Left matrix.</param>
        /// <param name="matrix">Right matrix.</param>
        /// <returns>New matrix as result of multiplication.</returns>
        public static Matrix3x3 Multiply(this Matrix3x3 original, Matrix3x3 matrix)
        {
            Matrix3x3 result = new Matrix3x3();

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    for (int k = 0; k < 3; k++)
                        result.Matrix[i][j] += original.Matrix[i][k] * matrix.Matrix[k][j];

            return result;
        }

        /// <summary>
        /// Multiplication matrix with vector <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="matrix">Matrix.</param>
        /// <param name="vector">Vector.</param>
        /// <returns>New vector as result of multipication.</returns>
        public static Vector3D Multiply(this Matrix3x3 matrix, Vector3D vector)
        {
            return new Vector3D
                    (
                        new Vector3D(matrix.Matrix[0][0], matrix.Matrix[0][1], matrix.Matrix[0][2]).DotProduct(vector),
                        new Vector3D(matrix.Matrix[1][0], matrix.Matrix[1][1], matrix.Matrix[1][2]).DotProduct(vector),
                        new Vector3D(matrix.Matrix[2][0], matrix.Matrix[2][1], matrix.Matrix[2][2]).DotProduct(vector)
                    );
        }

        /// <summary>
        /// Convert given matrix <paramref name="matrix"/> into vector of their euler angles.
        /// </summary>
        /// <param name="matrix">Rotation matrix.</param>
        /// <returns>Vector that represents matrix euler angles.</returns>
        public static Vector3D ToEulerAngles(this Matrix3x3 matrix)
        {
            var beta = Math.Asin(-matrix.Matrix[1][2]);

            var cosBeta = Math.Cos(beta);

            var alfa = Math.Atan2(matrix.Matrix[1][0] / cosBeta, matrix.Matrix[1][1] / cosBeta);

            var gamma = Math.Atan2(matrix.Matrix[0][2] / cosBeta, matrix.Matrix[2][2] / cosBeta);

            return new Vector3D(((float)beta).ToDegrees(), ((float)gamma).ToDegrees(), ((float)alfa).ToDegrees());
        }
    }

    /// <summary>
    /// Extension of <see cref="float"/> functionality.
    /// </summary>
    public static class FloatExtension
    {
        /// <summary>
        /// Convert value into radians.
        /// </summary>
        /// <param name="val">Converted value.</param>
        /// <returns>The value od <paramref name="val"/> in radians.</returns>
        public static float ToRadians(this float val)
        {
            return (float)(Math.PI / 180.0) * val;
        }

        /// <summary>
        /// Convert value into degrees.
        /// </summary>
        /// <param name="val">Converted value.</param>
        /// <returns>The value od <paramref name="val"/> in degrees.</returns>
        public static float ToDegrees(this float val)
        {
            return (float)(180.0 / Math.PI) * val;
        }

        /// <summary>
        /// Linear mapping of the value <paramref name="value"/> from the interval from <paramref name="from1"/> 
        /// to <paramref name="to1"/> to the return value from the interval from <paramref name="from2"/> to <paramref name="to2"/>.
        /// </summary>
        /// <param name="value">Value that is mapped.</param>
        /// <param name="from1">Begin of interval that contains <paramref name="value"/>.</param>
        /// <param name="to1">End of interval that contains <paramref name="value"/>.</param>
        /// <param name="from2">Begin of interval that contains desired remapped value.</param>
        /// <param name="to2">End of interval that contains desired remapped value.</param>
        /// <returns>Remapped value.</returns>
        public static float Map(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }

    /// <summary>
    /// Extension of <see cref="double"/> functionality.
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        /// Calculates the distance between list of values that describe a vector.
        /// To increase efficiency, a function <paramref name="difference"/> that counts 
        /// the difference between vector input points <typeparamref name="T"/> must be defined.
        /// </summary>
        /// <typeparam name="T">Components of vectors.</typeparam>
        /// <param name="original">First vector.</param>
        /// <param name="value">Second vector.</param>
        /// <param name="difference">The difference function between two <typeparamref name="T"/>.</param>
        /// <returns></returns>
        public static double Distance<T>(this IList<T> original, IList<T> value, Func<T, T, double> difference)
        {
            var result = 0d;

            for (var index = 0; index < original.Count; index++)
            {
                var diff = difference(value[index], original[index]);

                result += (diff * diff);
            }

            return Math.Sqrt(result);
        }
    }

    /// <summary>
    /// Extending the functionality of various collections.
    /// </summary>
    public static class CollectionExtension
    {
        /// <summary>
        /// Fills the input array <paramref name="array"/> with a value <paramref name="value"/>.
        /// </summary>
        /// <typeparam name="T">Type of array.</typeparam>
        /// <param name="array">Filled Array.</param>
        /// <param name="value">Filled value.</param>
        public static void Fill<T>(this T[] array, T value)
        {
            for (int i = 0; i < array.Length; i++) array[i] = value;
        }

        /// <summary>
        /// Fills the input array <paramref name="array"/> with a values from given <paramref name="value"/>.
        /// Individual values ​​are copied until they reach the end of one of the arrays.
        /// </summary>
        /// <typeparam name="T">Type of arrays.</typeparam>
        /// <param name="array">Filled array.</param>
        /// <param name="value">Filled values.</param>
        public static void Fill<T>(this T[] array, T[] value)
        {
            if (value != null) for (int i = 0; i < array.Length && i < value.Length; i++) array[i] = value[i];
        }

        /// <summary>
        /// Creates a new array and copies all values ​​of each array to it sequentially.
        /// </summary>
        /// <typeparam name="T">Type of array.</typeparam>
        /// <param name="arrays">List of joined arrays.</param>
        /// <returns>One array that contains the values ​​of all input arrays.</returns>
        public static T[] Join<T>(this IList<T[]> arrays)
        {
            var result = new T[arrays.Count * arrays[0].Length];

            for (var j = 0; j < arrays.Count; j++) Array.Copy(arrays[j], 0, result, j * arrays[j].Length, arrays[j].Length);

            return result;
        }
    }
}
