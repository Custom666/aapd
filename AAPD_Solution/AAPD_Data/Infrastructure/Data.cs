﻿using AAPD_Data.Enums;
using AAPD_Data.Extensions;
using AAPD_Data.Models;
using System.Text;

namespace AAPD_Data.Infrastructure
{
    /// <summary>
    /// Data from one particular type <see cref="DataType"/> of sensor at a specific time <see cref="Time"/>.
    /// </summary>
    public class Data
    {
        /// <value>
        /// The type of sensor from which the data <see cref="Vectors"/> are measured.
        /// </value>
        public DataType DataType { get; set; }

        /// <value>
        /// Measured motion data of <see cref="Vector3D"/>.
        /// </value>
        public Vector3D[] Vectors { get; set; }

        /// <value>
        /// The exact time the data was taken
        /// </value>
        public float Time { get; set; }

        /// <value>
        /// Specifies whether <see cref="Vectors"/> contains any data.
        /// </value>
        public bool IsEmpty() => Vectors.Length <= 0;

        /// <summary>
        /// Convert <see cref="Vectors"/> into one array of <see cref="float"/> sequentially.
        /// The sub-components of vectors are sequentially multiplied by input coefficients.
        /// </summary>
        /// <param name="coeficients">Multiplication coefficients.</param>
        /// <returns>One array of all vectors <see cref="Vectors"/> multiplicated by <paramref name="coeficients"/>.</returns>
        public float[] ToArray(float[] coeficients)
        {
            var coefs = new float[Vectors.Length * 3];

            coefs.Fill(1f);

            coefs.Fill(coeficients);

            for (int i = 0, j = 0; j < Vectors.Length; i += 3, j++)
            {
                coefs[i] *= Vectors[j].x;
                coefs[i + 1] *= Vectors[j].y;
                coefs[i + 2] *= Vectors[j].z;
            }

            return coefs;
        }

        /// <summary>
        /// String interpretation of data.
        /// </summary>
        /// <returns>String in following format 
        /// <c>
        /// time,Vectors[0].x,Vectors[0].y,Vectors[0].z,...,Vectors[Vectors.Length-1].x,Vectors[Vectors.Length-1].y,Vectors[Vectors.Length-1].z
        /// </c>
        /// </returns>
        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.Append(Time);

            foreach (var vector in Vectors) builder.Append($",{ vector }");

            return builder.ToString();
        }
    }
}
