﻿using AAPD_Data.Enums;
using AAPD_Data.Extensions;
using AAPD_Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace AAPD_Data.Infrastructure
{
    /// <summary>
    /// Collection of <see cref="Data"/>.
    /// 
    /// </summary>
    /// <remarks>
    /// <para>This collection can load measured data from text files and save them into text file.</para>
    /// <para>Data in this collection can be translated and rotated.</para>
    /// </remarks>
    /// <typeparam name="T">Type of data <see cref="Data"/>.</typeparam>
    public class DataCollection<T> where T : Data, new()
    {
        /// <value>
        /// List of data <typeparamref name="T"/>.
        /// </value>
        public IList<T> Items { get; private set; } = new List<T>();

        /// <summary>
        /// Returns an instance of a new data collection <see cref="DataCollection{T}"/> from a specific senzor type <paramref name="dataType"/>. 
        /// Data is retrieved from the input file <paramref name="filename"/>.
        /// </summary>
        /// <param name="filename">Path to file with raw data.</param>
        /// <param name="dataType">Type of senzor.</param>
        /// <returns>Collection of specfic data loaded from file.</returns>
        public static DataCollection<T> LoadData(string filename, DataType dataType)
        {
            var result = new DataCollection<T>();

            result.Load(filename, dataType);

            return result;
        }

        /// <summary>
        /// Loaded a specific type of <paramref name="dataType"/> raw data from file <paramref name="filename"/>.
        /// </summary>
        /// <param name="filename">Path to file with raw data.</param>
        /// <param name="dataType">Type of senzor.</param>
        public virtual void Load(string filename, DataType dataType)
        {
            foreach (var line in File.ReadAllLines(filename))
            {
                var parameters = System.Text.RegularExpressions.Regex.Split(line, "[ ;]+");

                if (!TimeSpan.TryParse(parameters[0], CultureInfo.InvariantCulture, out var time)) throw new Exception($"Parameter [{ parameters[0] }] cannot be parse as time!");

                if (!float.TryParse(parameters[1], NumberStyles.Any, CultureInfo.InvariantCulture, out var x)) throw new Exception($"Parameter [{ parameters[1] }] cannot be parse as x-position!");
                if (!float.TryParse(parameters[2], NumberStyles.Any, CultureInfo.InvariantCulture, out var y)) throw new Exception($"Parameter [{ parameters[2] }] cannot be parse as y-position!");
                if (!float.TryParse(parameters[3], NumberStyles.Any, CultureInfo.InvariantCulture, out var z)) throw new Exception($"Parameter [{ parameters[3] }] cannot be parse as z-position!");
                if (!float.TryParse(parameters[4], NumberStyles.Any, CultureInfo.InvariantCulture, out var rx)) throw new Exception($"Parameter [{ parameters[4] }] cannot be parse as x-rotation!");
                if (!float.TryParse(parameters[5], NumberStyles.Any, CultureInfo.InvariantCulture, out var ry)) throw new Exception($"Parameter [{ parameters[5] }] cannot be parse as y-rotation!");
                if (!float.TryParse(parameters[6], NumberStyles.Any, CultureInfo.InvariantCulture, out var rz)) throw new Exception($"Parameter [{ parameters[6] }] cannot be parse as z-rotation!");

                Items.Add(new T
                {
                    DataType = dataType,
                    Time = (time.Minutes * 60f) + time.Seconds + (time.Milliseconds / 1000f),
                    Vectors = new Vector3D[] 
                    {
                        new Vector3D(x, y, z),
                        new Vector3D(rx, ry, rz)
                    }
                });
            }
        }

        /// <summary>
        /// Save current content of <see cref="Items"/> into file <paramref name="filename"/>.
        /// Data are writen sequentially and each data is formated with their <c>ToString()</c> method.
        /// </summary>
        /// <param name="filename"></param>
        public virtual void Save(string filename)
        {
            using (var writer = new StreamWriter(filename, false)) foreach (var d in Items) writer.WriteLine(d);
        }

        /// <summary>
        /// Applying position translation to all <see cref="Items"/> data by <paramref name="vector"/>.
        /// </summary>
        /// <param name="vector"></param>
        public virtual void Translate(Vector3D vector)
        {
            for (int index = 0; index < Items.Count; ++index) Items[index].Vectors[0] += vector; 
        }

        /// <summary>
        /// Applying rotation to all <see cref="Items"/> data.
        /// </summary>
        /// <param name="angle">Rotation angle in degrees.</param>
        /// <param name="axis">Rotation axis.</param>
        /// <param name="centerPoint">The point around which data is rotates.</param>
        public virtual void Rotate(float angle, Vector3D axis, Vector3D centerPoint)
        {
            // craete rotation matrix
            var rotationMatrix = Matrix3x3.CreateRotation(axis, angle);

            // rotate each data around center point
            for (int index = 0; index < Items.Count; ++index)
            {
                var position = Items[index].Vectors[0] - centerPoint; // translate to origin

                var newPosition = rotationMatrix.Multiply(position) + centerPoint; // rotate and translate to center point

                var eulerRotationMatrix = Matrix3x3.CreateFromEulerAngles(Items[index].Vectors[1], "ZXY");

                Items[index].Vectors[1] = rotationMatrix.Multiply(eulerRotationMatrix).ToEulerAngles();
                Items[index].Vectors[0] = newPosition;
            }
        }
    }
}
