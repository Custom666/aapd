﻿namespace AAPD_Data.Enums
{
    /// <summary>
    /// The human body area on which the sensors that produce motion data are located.
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Head area.
        /// </summary>
        Head = 0,

        /// <summary>
        /// Chest area.
        /// </summary>
        Chest = 1,

        /// <summary>
        /// Hand area.
        /// </summary>
        Forearm = 2,

        /// <summary>
        /// Arm area.
        /// </summary>
        Arm = 3,

        /// <summary>
        /// Unknown area.
        /// </summary>
        Unknown = 4
    }
}
