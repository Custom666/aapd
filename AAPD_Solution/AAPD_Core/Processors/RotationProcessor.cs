﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Infrastructure;
using AAPD_Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAPD_Core.Processors
{
    public class RotationProcessor : IDataProcessor<Data>
    {
        private List<Matrix3x3>[] frames = new List<Matrix3x3>[4];

        private int size;

        private readonly double ANGLE_PERCENT = Math.PI * 4; 

        private Data[] lastData = new Data[4];

        public void Build(AAPD_Data.Infrastructure.DataCollection<Data>[] collection)
        {
            foreach (DataCollection<Data> dataColl in collection)
            {
                int i = (int)dataColl.Items[0].DataType;
                frames[i] = new List<Matrix3x3>();
                foreach (Data data in dataColl.Items)
                {
                    if (!data.IsEmpty())
                    {
                        Matrix3x3 mat = Matrix3x3.CreateFromEulerAngles(data.Vectors[1], "");
                        frames[i].Add(mat);
                       // Console.WriteLine(String.Format("{0}, {1}, {2}", mat.Matrix[0][0], mat.Matrix[1][1], mat.Matrix[2][2]));
                        
                    }
                }
            }
            size = collection[0].Items.Count;
        }

        public void Input(Data data)
        {
            int index = (int)data.DataType;
            lastData[index] = data;
        }

        public void Process(out float phase, out float quality)
        {
            int bestIndex = -1;
            double bestDist = double.MaxValue;
            for (int i = 0; i < size; i++)
            {
                double d = 0;
                for (int j = 0; j < 4; j++)
                {
                    Matrix3x3 b = Matrix3x3.CreateFromEulerAngles(lastData[j].Vectors[1], "");
                    d += getAngle(frames[j][i], b);
                }
                if(d < bestDist)
                {
                    bestDist = d;
                    bestIndex = i;
                }
            }
            phase = bestIndex / (float)size;
            quality = (float) (1-(bestDist / ANGLE_PERCENT));
        }


        private double getAngle(Matrix3x3 a, Matrix3x3 b)
        {

            Matrix3x3 bt = new Matrix3x3();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    bt.Matrix[j][i] = b.Matrix[i][j];
                }
            }
            double d = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    d += a.Matrix[i][j] * bt.Matrix[j][i];
                }
            }

            
            double res = Math.Acos((d-1) / 2);
            return Math.Abs(res);
        }

        public (float, float, float, float) Distances =>(0f, 0f, 0f, 0f);

        public (float[], float[], float[], float[]) NearestPoints => (null, null, null, null);
    }
}
