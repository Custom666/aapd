﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Extensions;
using AAPD_Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;

namespace AAPD_Core.Processors
{
    /// <summary>
    /// Data processor that work with mock data in text file formated as [time,phase,quality]. Each formatted data are in new line. 
    /// Processor do simple linear interpolation between values from mock file based on 0.1s time incrementation.
    /// </summary>
    public class FileDataProcessor : IDataProcessor<Data>
    {
        private List<(float, float, float)> _data = new List<(float, float, float)>();

        private ((float, float, float), (float, float, float)) _interval;

        private readonly float _density = .1f;
        private int _index = 1;
        private float _currentTime;

        public (float, float, float, float) Distances => throw new NotImplementedException();

        public (float[], float[], float[], float[]) NearestPoints => throw new NotImplementedException();

        /// <summary>
        /// Creates a new processor of tagged data from the file.
        /// </summary>
        /// <param name="path">File path with tagged data.</param>
        public FileDataProcessor(string path)
        {
            foreach (var line in File.ReadAllLines(path))
            {
                var values = line.Split(',');

                _data.Add((float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2])));
            }

            reset();
        }

        private void reset()
        {
            _index = 1;

            _interval.Item1 = _data[0];
            _interval.Item2 = _data[1];

            _currentTime = _interval.Item1.Item1;
        }

        /// <summary>
        /// Reset current time position and start analyzator from beginning of file with marked data.
        /// </summary>
        /// <param name="collection">Can be null in this scenario.</param>
        public void Build(DataCollection<Data>[] collection)
        {
            reset();
        }

        /// <summary>
        /// This data processor work with mock data from file. This method is not implemented!
        /// </summary>
        /// <param name="data">Can be null.</param>
        public void Input(Data data)
        {
            // does not make sence
        }

        /// <summary>
        /// It produces phase and quality by linearly interpolating the tagged data according to intervals from the tagged file.
        /// Each time this method is called, the interpolated time is shifted by 0.1 seconds.
        /// </summary>
        /// <param name="phase">Current phase value.</param>
        /// <param name="quality">Current quality value.</param>
        public void Process(out float phase, out float quality)
        {
            phase = quality = -1;

            _currentTime += _density;

            if (_currentTime >= _interval.Item2.Item1)
            {
                _interval.Item1 = _interval.Item2;

                _index++;

                if (_index > _data.Count - 1) reset();
                else _interval.Item2 = _data[_index];
            }

            // map value from time interval into phase and quality intervals
            phase = _currentTime.Map(_interval.Item1.Item1, _interval.Item2.Item1, _interval.Item1.Item2, _interval.Item2.Item2);
            quality = _currentTime.Map(_interval.Item1.Item1, _interval.Item2.Item1, _interval.Item1.Item3, _interval.Item2.Item3);
        }
    }
}
