﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Enums;
using AAPD_Data.Extensions;
using AAPD_Data.Infrastructure;
using KDTree;
using System.Collections.Generic;
using System.Linq;

namespace AAPD_Core.Processors
{
    /// <summary>
    /// A data processor that analyzes the data <see cref="Data"/> that is actually inserted based on data learned.
    /// </summary>
    public class DataProcessor : IDataProcessor<Data>
    {
        /// <value>
        /// Dimension of k-dimensional tree <see cref="KDTree.KDTree{T}"/>.
        /// </value>
        public readonly int Dimension;

        private KDTree<float> _tree;

        private Data[] _data;

        private System.Func<float, float, double> _distanceFunc = (x, y) => x - y;
        private (float, float, float, float) _distances;
        private (float[], float[], float[], float[]) _nearestPoints;

        /// <summary>
        /// Creates a new specified dimension processor with a default data difference calculation function that is described as their difference.
        /// </summary>
        /// <param name="dimension">Dimension of k-dimensional tree <see cref="KDTree.KDTree{T}"/>.</param>
        public DataProcessor(int dimension)
        {
            Dimension = dimension;

            _data = new Data[4];

            _tree = new KDTree<float>(Dimension, _distanceFunc);
        }

        /// <summary>
        /// Creates a new specified dimension processor with a default data difference calculation function that is described as their difference.
        /// Data processor is build <see cref="Build(DataCollection{Data}[])"/> from input data collections <paramref name="dataCollection"/>.
        /// </summary>
        /// <param name="dimension">Dimension of k-dimensional tree <see cref="KDTree.KDTree{T}"/>.</param>
        /// <param name="dataCollection">Collection of data from individual types of sensors.</param>
        public DataProcessor(int dimension, DataCollection<Data>[] dataCollection) : this(dimension)
        {
            Build(dataCollection);
        }

        public (float, float, float, float) Distances => _distances;

        public (float[], float[], float[], float[]) NearestPoints => _nearestPoints;

        /// <summary>
        /// Builds a tree <see cref="KDTree.KDTree{T}"/> with <see cref="Dimension"/> from input data collections.
        /// </summary>
        /// <param name="collection">Collection of data from individual types of sensors.</param>
        public void Build(DataCollection<Data>[] collection)
        {
            _tree.GenerateTree(createTunnelData(collection));
        }

        /// <summary>
        /// The last data from each sensor type <see cref="DataType"/> is always collected.
        /// </summary>
        /// <param name="data">Data from sensor type <see cref="DataType"/>.</param>
        public void Input(Data data)
        {
            _data[(int)data.DataType] = data;
        }

        /// <summary>
        /// It analyzes the last collected data through method <see cref="Input(Data)"/>.
        /// It produces their quality and phase based on data learned.
        /// </summary>
        /// <param name="phase">The phase of motion described by the input data <see cref="Data"/>.</param>
        /// <param name="quality">The quality of motion described by the input data <see cref="Data"/>.</param>
        public void Process(out float phase, out float quality)
        {
            foreach (var row in _data) if (row == null || row.IsEmpty()) throw new System.Exception("Not enought data to process. Please insert more different data.");

            var value = _data.Select(x => x.ToArray(Settings.Coefficients[x.DataType])).ToList().Join();

            var nearest = _tree.Search(value);

            phase = nearest.DistanceFromRoot.Map(0f, _tree.TotalDistance, 0f, 1f);

            _nearestPoints.Item1 = new float[] { nearest.Value[0], nearest.Value[1], nearest.Value[2] };
            _nearestPoints.Item2 = new float[] { nearest.Value[6], nearest.Value[7], nearest.Value[8] };
            _nearestPoints.Item3 = new float[] { nearest.Value[12], nearest.Value[13], nearest.Value[14] };
            _nearestPoints.Item4 = new float[] { nearest.Value[18], nearest.Value[19], nearest.Value[20] };

            _distances.Item1 = (float) new float[] { value[0], value[1], value[2] }.Distance(_nearestPoints.Item1, _distanceFunc);
            _distances.Item2 = (float) new float[] { value[6], value[7], value[8] }.Distance(_nearestPoints.Item2, _distanceFunc);
            _distances.Item3 = (float) new float[] { value[12], value[13], value[14] }.Distance(_nearestPoints.Item3, _distanceFunc);
            _distances.Item4 = (float) new float[] { value[18], value[19], value[20] }.Distance(_nearestPoints.Item4, _distanceFunc);

            var distance = (float) nearest.Value.Distance(value, _distanceFunc);

            quality = distance <= Settings.TunnelRadius ? 1f : 
                      distance > Settings.MaximumQualityRange + Settings.TunnelRadius ? 0f : 
                      distance.Map(Settings.TunnelRadius, Settings.TunnelRadius + Settings.MaximumQualityRange, 1f, 0f);
        }

        private IList<float[]> createTunnelData(DataCollection<Data>[] data)
        {
            var result = new List<float[]>();
            
            for (var i = 0; i < data[0].Items.Count; i++)
            {
                var row = new List<float[]>();
                
                for (var j = 0; j < data.Length; j++)
                    row.Add(data[j].Items[i].ToArray(Settings.Coefficients[data[j].Items[i].DataType]));

                result.Add(row.Join());
            }

            return result;
        }
    }
}
