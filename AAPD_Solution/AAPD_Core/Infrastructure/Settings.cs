﻿using AAPD_Data.Enums;
using System.Collections.Generic;

namespace AAPD_Core.Infrastructure
{
    /// <summary>
    /// Library setting constants.
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Multiplication coefficients of individual vectors describing the motion and rotation of each sensor type <see cref="DataType"/>.
        /// <example>
        /// <code>
        /// Settings.Coefficients[DataType.Head][0] = 0.1f;
        /// Settings.Coefficients[DataType.Head] = new float[] { 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f };
        /// </code>
        /// </example>
        /// </summary>
        public static readonly IDictionary<DataType, float[]> Coefficients = new Dictionary<DataType, float[]>
        {
            {
                DataType.Head,
                new float[]
                {
                    1f, 1f, 1f, // position
                    1f, 1f, 1f, // rotation
                }
            },
            {
                DataType.Chest,
                new float[]
                {
                    1f, 1f, 1f, // position
                    1f, 1f, 1f, // rotation
                }
            },
            {
                DataType.Arm,
                new float[]
                {
                    1f, 1f, 1f, // position
                    1f, 1f, 1f, // rotation
                }
            },
            {
                DataType.Forearm,
                new float[]
                {
                    1f, 1f, 1f, // position
                    1f, 1f, 1f, // rotation
                }
            },
        };

        /// <summary>
        /// Radius of the build tunnel <see cref="Tunnel{T}"/>.
        /// </summary>
        public static float TunnelRadius { get; set; } = 1f;

        /// <summary>
        /// Maximum possible distance value to which the processor <see cref="IDataProcessor{T}"/> produces non-zero <c>quality</c> results.
        /// </summary>
        public static float MaximumQualityRange { get; set; } = 1f;

        /// <summary>
        /// Ration distance from head to forearm between user and trainer.
        /// </summary>
        public static float DistanceRationCoeficient { get; set; } = 1f;
    }
}
