﻿using AAPD_Data.Enums;
using AAPD_Data.Extensions;
using AAPD_Data.Infrastructure;
using AAPD_Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AAPD_Core.Infrastructure
{
    /// <summary>
    /// A tunnel around the test data <see cref="Data"/> that describes the user's movement.
    /// A tunnel can transform all data collections <see cref="DataCollection{T}"/> based 
    /// on data <see cref="Data"/> from one particular type <see cref="DataType"/> of sensor 
    /// and be displayed around the sampled data <see cref="RawData"/> of any type of sensor.
    /// </summary>
    /// <typeparam name="T">Type of data <see cref="Data"/></typeparam>
    public class Tunnel<T> where T : Data, new()
    {
        /// <value>
        /// The value by which the data <see cref="RawData"/> is sampled. 
        /// N-th is always selected.
        /// </value>
        public int SampleDensity { get; set; } = 10;

        /// <value>
        /// A value that indicates whether the processor is ready for analysis.
        /// </value>
        public bool CanProcess { get; private set; }

        /// <value>
        /// Processor that analyze phase and quality of movement based on data <typeparamref name="T"/>. 
        /// <para>
        /// When this property is get and the processor is not ready for analysis, then the processor will
        /// be <c>Build</c> from its current data and property <see cref="CanProcess"/> is set to True.
        /// </para>
        /// <para>
        /// When this property is set, the property <see cref="CanProcess"/> value is changed to False.
        /// </para>
        /// </value>
        public IDataProcessor<T> Processor
        {
            get
            {
                if(!CanProcess)
                {
                    _processor.Build(_originalData);

                    CanProcess = true;
                }

                return _processor;
            }
            set
            {
                _processor = value;

                CanProcess = false;
            }
        }

        private IDataProcessor<T> _processor;
        public readonly DataCollection<T>[] _originalData;
        private Data[] _neutrals;


        /// <value>
        /// Sampled collections of current data <typeparamref name="T"/> that surround the tunnel.
        /// Data is sampled based on <see cref="SampleDensity"/> property value.
        /// </value>
        public IEnumerable<T>[] RawData => _originalData.Select(collection => collection.Items.Where((x, i) => i % SampleDensity == 0)).AsEnumerable().ToArray();

        /// <summary>
        /// Creates a tunnel around the input data collections <see cref="DataCollection{T}"/>.
        /// </summary>
        /// <param name="collection">Collection of data from individual types of sensors.</param>
        public Tunnel(DataCollection<T>[] collection)
        {
            for (int i = 0; i < collection.Length; i++)
                for (var j = 0; j < collection[i].Items.Count; j++)
                    collection[i].Items[j].Vectors[0] *= Settings.DistanceRationCoeficient;

            _originalData = collection;
        }

        /// <summary>
        /// Creates a tunnel around the input data collections <see cref="DataCollection{T}"/> with <see cref="IDataProcessor{T}"/> processor.
        /// </summary>
        /// <param name="collection">Collection of data from individual types of sensors.</param>
        /// <param name="processor">Processor that analysis phase and quality of movement based on data <typeparamref name="T"/>.</param>
        public Tunnel(DataCollection<T>[] collection, IDataProcessor<T> processor) : this(collection)
        {
            _processor = processor;
        }


        public void readNeutral(string path) {
            _neutrals = new Data[4];
            foreach(string line in File.ReadAllLines(path)) {
                string[] parameters = Regex.Split(line, "[ ;]+");

                DataType type = (DataType) Enum.Parse(typeof(DataType), parameters[0]);

                if (!float.TryParse(parameters[1], NumberStyles.Any, CultureInfo.InvariantCulture, out var x)) throw new Exception($"Parameter [{ parameters[1] }] cannot be parse as x-position!");
                if (!float.TryParse(parameters[2], NumberStyles.Any, CultureInfo.InvariantCulture, out var y)) throw new Exception($"Parameter [{ parameters[2] }] cannot be parse as y-position!");
                if (!float.TryParse(parameters[3], NumberStyles.Any, CultureInfo.InvariantCulture, out var z)) throw new Exception($"Parameter [{ parameters[3] }] cannot be parse as z-position!");
                if (!float.TryParse(parameters[4], NumberStyles.Any, CultureInfo.InvariantCulture, out var rx)) throw new Exception($"Parameter [{ parameters[4] }] cannot be parse as x-rotation!");
                if (!float.TryParse(parameters[5], NumberStyles.Any, CultureInfo.InvariantCulture, out var ry)) throw new Exception($"Parameter [{ parameters[5] }] cannot be parse as y-rotation!");
                if (!float.TryParse(parameters[6], NumberStyles.Any, CultureInfo.InvariantCulture, out var rz)) throw new Exception($"Parameter [{ parameters[6] }] cannot be parse as z-rotation!");


                _neutrals[(int)type] = new T {
                    DataType = type,
                    Time = 0,
                    Vectors = new Vector3D[]
                  {
                        new Vector3D(x, y, z),
                        new Vector3D(rx, ry, rz)
                  }
                };
            }
        }

        /// <summary>
        /// Transform all data collections <see cref="DataCollection{T}"/> based on input 
        /// data <paramref name="destinationPoint"/> position and sensor type <see cref="DataType"/>.
        /// Collections are transformed by the first collection data from the same sensor type.
        /// </summary>
        /// <param name="destinationPoint">Data to which all tunnel data collections are transformed.</param>
        public double TransformTo(T destinationPoint)
        {
            var originalPoint = _neutrals[(int)destinationPoint.DataType];
            //var originalPoint = _originalData.First(collection => collection.Items.Any(rawData => rawData.DataType == destinationPoint.DataType)).Items[0];

            //Vector3D v =  originalPoint.Vectors[0] - _neutrals[(int)destinationPoint.DataType].Vectors[0];
            //v += destinationPoint.Vectors[0];
            Matrix3x3 origMatix = Matrix3x3.CreateFromEulerAngles(originalPoint.Vectors[1], null);

            var origZ = origMatix.Multiply(new Vector3D(0, 0, 1));

            Matrix3x3 currentMatix = Matrix3x3.CreateFromEulerAngles(destinationPoint.Vectors[1], null);

            var currentZ = currentMatix.Multiply(new Vector3D(0, 0, 1));

            double alpha = (Math.Atan2(currentZ.x, currentZ.z) - Math.Atan2(origZ.x, origZ.z))/Math.PI*180;




            // translation distance to world origin
            var originalDistance = originalPoint.Vectors[0] * -1;

            // calculate alpha as the smalest angle between original point and destination point
            // look at data in two dimension
            /*var x = new Vector3D(destinatinPoint.Vectors[0].x, 0, destinatinPoint.Vectors[0].z);
            var y = new Vector3D(originalPoint.Vectors[0].x, 0, originalPoint.Vectors[0].z);

            var alpha = ((float)Math.Acos((x.DotProduct(y) / (x.SqrtMagnitude() * y.SqrtMagnitude())).ToRadians())).ToDegrees();*/

            // transform all data
            for (int i = 0; i < _originalData.Length; i++)
            {
                // translate original data to world origin
                _originalData[i].Translate(originalDistance);

                // rotate around Y by alpha
                _originalData[i].Rotate((float)alpha, Vector3D.Y, Vector3D.Empty);

                // translate to desired point
                _originalData[i].Translate(destinationPoint.Vectors[0]);
            }

            return alpha;
        }
    }
}
