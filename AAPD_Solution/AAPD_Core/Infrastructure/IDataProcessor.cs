﻿using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;

namespace AAPD_Core.Infrastructure
{
    /// <summary>
    /// Analyze phase and quality of movement based on data <typeparamref name="T"/>. 
    /// Processor should be intialized through <c>Build</c> method. 
    /// </summary>
    /// <typeparam name="T">Type of data <see cref="Data"/>.</typeparam>
    public interface IDataProcessor<T> where T : Data, new()
    {
        /// <summary>
        /// Teaches analyzer based on data collections <see cref="DataCollection{T}"/>. 
        /// Use this for initialization.
        /// </summary>
        /// <param name="collection">Arrays of data collections <see cref="DataCollection{T}"/> of individual types <see cref="DataType"/> of sensors.</param>
        void Build(DataCollection<T>[] collection);

        /// <summary>
        /// Collects current motion input data <typeparamref name="T"/> for upcoming evaluation by method <c>Process</c>.
        /// </summary>
        /// <param name="data">Current captured data.</param>
        void Input(T data);

        /// <summary>
        /// Analyze the collected data and produce its phase and quality.
        /// </summary>
        /// <param name="phase"></param>
        /// <param name="quality"></param>
        void Process(out float phase, out float quality);

        /// <summary>
        /// Remove this...
        /// </summary>
        (float, float, float, float) Distances { get; }

        /// <summary>
        /// And this...
        /// </summary>
        (float[], float[], float[], float[]) NearestPoints { get; }
    }
}
