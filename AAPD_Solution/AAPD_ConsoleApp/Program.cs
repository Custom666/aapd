﻿using AAPD_Core.Infrastructure;
using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using AAPD_Core.Processors;
using KDTree;
using KDTree.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using AAPD_Data.Models;

namespace AAPD_ConsoleApp
{
    public class Program
    {
        private static readonly DataCollection<Data>[] _trainCollection = new Dictionary<DataType, string>
        {
           //{ DataType.Head, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUp\Head.txt" },
           // { DataType.Chest, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUp\Chest.txt" },
           // { DataType.Forearm, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUp\Forearm.txt" },
           // { DataType.Arm, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUp\Arm.txt" },
        }.Select(file => DataCollection<Data>.LoadData(file.Value, file.Key)).ToArray();

        private static readonly DataCollection<Data>[] _testCollection = new Dictionary<DataType, string>
        {
           //{ DataType.Head, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUpDown\Head.txt" },
           // { DataType.Chest, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUpDown\Chest.txt" },
           // { DataType.Forearm, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUpDown\Forearm.txt" },
           // { DataType.Arm, @"D:\Workspace\UNITY\AAPD\TestData\Vive\Correct\ThirdCorrectMoveUpDown\Arm.txt" },
        }.Select(file => DataCollection<Data>.LoadData(file.Value, file.Key)).ToArray();

        //private static IDataProcessor<VRData> _processor;

        public static void Main(string[] args)
        {
            //SaveTestData();
            Console.WriteLine("start");
            ProcessorTest();
            Console.WriteLine("end");
      
        }

        private static void ProcessorTest()
        {
            var collections = new DataCollection<Data>[]
            {
                DataCollection<Data>.LoadData("C:/Users/Grafici/Desktop/poorv/AAPD/data/Head.csv", DataType.Head),
                DataCollection<Data>.LoadData("C:/Users/Grafici/Desktop/poorv/AAPD/data/Chest.csv", DataType.Chest),
                DataCollection<Data>.LoadData("C:/Users/Grafici/Desktop/poorv/AAPD/data/Forearm.csv", DataType.Forearm),
                DataCollection<Data>.LoadData("C:/Users/Grafici/Desktop/poorv/AAPD/data/Arm.csv", DataType.Arm)
            };
            RotationProcessor rotationProcessor = new RotationProcessor();

            rotationProcessor.Build(collections);

            Data chest = new Data();
            chest.DataType = DataType.Chest;
            chest.Vectors = new Vector3D[2];
            chest.Vectors[1] = new Vector3D((float)150, (float)160.96527, (float)180.801282);

            Data arm = new Data();
            arm.DataType = DataType.Arm;
            arm.Vectors = new Vector3D[2];
            arm.Vectors[1] = new Vector3D((float)190.6977, (float)56.3189, (float)220.335);

            Data foreArm = new Data();
            foreArm.DataType = DataType.Forearm;
            foreArm.Vectors = new Vector3D[2];
            foreArm.Vectors[1] = new Vector3D((float)15.28946, (float)59.1353, (float)240.0677);

            Data head = new Data();
            head.DataType = DataType.Head;
            head.Vectors = new Vector3D[2];
            head.Vectors[1] = new Vector3D((float)80.9105, (float)320.03635, (float)66.277);


            rotationProcessor.Input(chest);
            rotationProcessor.Input(arm);
            rotationProcessor.Input(foreArm);
            rotationProcessor.Input(head);

            float quality, phase;

            rotationProcessor.Process(out phase, out quality);


            Console.WriteLine("PHASE: {0}, QUALITY: {1}", phase, quality);

            

        }


        

      
    }
}
