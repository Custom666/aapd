﻿using AAPD_Core.Infrastructure;
using AAPD_Core.Processors;
using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using AAPD_Data.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DataController : MonoBehaviour
{
    [SerializeField] private DataType _relativeTo;

    [SerializeField] private Transform _head;
    [SerializeField] private Transform _chest;
    [SerializeField] private Transform _arm;
    [SerializeField] private Transform _forearm;

    [SerializeField] private Text _phaseText;
    [SerializeField] private Text _qualityText;

    private IDictionary<DataType, string> _dataFilePath = new Dictionary<DataType, string>
    {
        { DataType.Head, @"d:\Workspace\UNITY\AAPD\TestData\Vive\Correct\Head_SecondCorrectMove.txt" },
        { DataType.Chest, @"d:\Workspace\UNITY\AAPD\TestData\Vive\Correct\Chest_SecondCorrectMove.txt" },
        { DataType.Forearm, @"d:\Workspace\UNITY\AAPD\TestData\Vive\Correct\Forearm_SecondCorrectMove.txt" },
        { DataType.Arm, @"d:\Workspace\UNITY\AAPD\TestData\Vive\Correct\Arm_SecondCorrectMove.txt" },
    };

    private Tunnel<Data> _tunnel;
    private bool _canProceedInput;

    private void Start()
    {
        _tunnel = new Tunnel<Data>(_dataFilePath.Select(x => DataCollection<Data>.LoadData(x.Value, x.Key)).ToArray(), new DataProcessor(24));
    }

    private void Update()
    {
        if (!_canProceedInput && Input.GetKey(KeyCode.Space))
        {
            transformData();

            _canProceedInput = true;
        }
        else if (_canProceedInput)
        {
            _tunnel.Processor.Input(createVRData(DataType.Head, _head));
            _tunnel.Processor.Input(createVRData(DataType.Chest, _chest));
            _tunnel.Processor.Input(createVRData(DataType.Arm, _arm));
            _tunnel.Processor.Input(createVRData(DataType.Forearm, _forearm));
        }
    }

    private void transformData()
    {
        for (int index = 0; index < _tunnel.RawData.Length; index++)
        {

        }
    }

    private void FixedUpdate()
    {
        if (!_canProceedInput) return;

        _tunnel.Processor.Process(out var phase, out var quality);

        _phaseText.text = $"{ _tunnel.GetType().Name } phase     { phase.ToString("P4", CultureInfo.InvariantCulture)}";
        _qualityText.text = $"{ _tunnel.GetType().Name } quality     { quality.ToString("P4", CultureInfo.InvariantCulture)}";
    }

    private Data createVRData(DataType type, Transform from) => new Data
    {
        DataType = type,
        Vectors = new[]
        {
            new Vector3D(from.position.x, from.position.y, from.position.z),
            new Vector3D(from.localEulerAngles.x, from.localEulerAngles.y, from.localEulerAngles.z)
        }
    };
}
