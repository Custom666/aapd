﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animation))]
public class AnimationDataMarker : MonoBehaviour
{
    [SerializeField] private float _phase;
    [SerializeField] private float _quality;

    [SerializeField] private bool _extractAnimation;

    [SerializeField] private Text _phaseText;
    [SerializeField] private Text _qualityText;

    private Animation _animation;

    // Use this for initialization
    private void Start()
    {
        _animation = GetComponent<Animation>();

        if(_extractAnimation) ExtractAnimation("Assets/markedData.txt");
    }

    private void Update()
    {
        _phaseText.text = $"Phase       { _phase.ToString("P4", CultureInfo.InvariantCulture) }";
        _qualityText.text = $"Quality       { _quality.ToString("P4", CultureInfo.InvariantCulture) }";
    }

    public void ExtractAnimation(string path)
    {
        var phaseKeyFrames = AnimationUtility.GetEditorCurve(_animation.clip,
            AnimationUtility.GetCurveBindings(_animation.clip)
            .Where(x => x.propertyName == "_phase")
            .First()).keys;

        var qualityKeyFrames = AnimationUtility.GetEditorCurve(_animation.clip,
            AnimationUtility.GetCurveBindings(_animation.clip)
            .Where(x => x.propertyName == "_quality")
            .First()).keys;

        var data = new StringBuilder();

        for (var i = 0; i < qualityKeyFrames.Length; i++)
        {
            var phase = phaseKeyFrames[i].value;
            var quality = qualityKeyFrames[i].value;
            var time = (phaseKeyFrames[i].time + qualityKeyFrames[i].time) / 2f;

            data.AppendLine($"{ time.ToString(CultureInfo.InvariantCulture) },{ phase.ToString(CultureInfo.InvariantCulture) },{ quality.ToString(CultureInfo.InvariantCulture) }");
        }

        File.WriteAllText(path, data.ToString());
    }
}
