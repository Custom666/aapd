﻿using AAPD_Data.Enums;
using AAPD_Data.Infrastructure;
using AAPD_Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEditor;
using UnityEngine;

public struct ViveNode
{
    public float time;
    public float localPositionX;
    public float localPositionY;
    public float localPositionZ;
    public float localRotationX;
    public float localRotationY;
    public float localRotationZ;
}

public class AnimationEditor : MonoBehaviour
{
    [SerializeField] private float _angle = 45f;
    [SerializeField] private Vector3 _axis = Vector3.up;
    [SerializeField] private Vector3 _centerPoint = Vector3.zero;

    private Vector3D axis => new Vector3D(_axis.x, _axis.y, _axis.z);
    private Vector3D centerPoint => new Vector3D(_centerPoint.x, _centerPoint.y, _centerPoint.z);

    void LoadData(string data, AnimationClip clip)
    {
        string filename = "Assets/" + data + ".txt";
        List<ViveNode> result = new List<ViveNode>();
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        var lines = System.IO.File.ReadAllLines(filename);
        int timespan = 1000;
        var lastTime = -timespan;
        foreach (var line in lines)
        {
            var tokens = Regex.Split(line, "[ ,]+");
            var time = TimeSpan.Parse(tokens[0]).TotalMilliseconds;
            if (time < 50000) continue;
            time -= 50000;
            if (time > lastTime + timespan)
            {
                lastTime = ((int)(time / timespan)) * timespan;
                var node = new ViveNode()
                {
                    time = (float)(time / 1000.0),
                    localPositionX = float.Parse(tokens[1]),
                    localPositionY = float.Parse(tokens[2]),
                    localPositionZ = float.Parse(tokens[3]),
                    localRotationX = float.Parse(tokens[4]),
                    localRotationY = float.Parse(tokens[5]),
                    localRotationZ = float.Parse(tokens[6]),
                };
                result.Add(node);
                Debug.Log(node.time);
            }

            Keyframe[] posX = new Keyframe[result.Count];
            Keyframe[] posY = new Keyframe[result.Count];
            Keyframe[] posZ = new Keyframe[result.Count];
            Keyframe[] rotX = new Keyframe[result.Count];
            Keyframe[] rotY = new Keyframe[result.Count];
            Keyframe[] rotZ = new Keyframe[result.Count];
            Keyframe[] rotW = new Keyframe[result.Count];

            for (int i = 0; i < posX.Length; i++)
            {
                posX[i] = new Keyframe(result[i].time, result[i].localPositionX);
                posY[i] = new Keyframe(result[i].time, result[i].localPositionY);
                posZ[i] = new Keyframe(result[i].time, result[i].localPositionZ);
                Quaternion rot = Quaternion.Euler(result[i].localRotationX, result[i].localRotationY, result[i].localRotationZ);
                rotX[i] = new Keyframe(result[i].time, rot.x);
                rotY[i] = new Keyframe(result[i].time, rot.y);
                rotZ[i] = new Keyframe(result[i].time, rot.z);
                rotW[i] = new Keyframe(result[i].time, rot.w);
            }

            for (int i = 1; i < posX.Length - 1; i++)
            {
                posX[i].inTangent = (posX[i + 1].value - posX[i - 1].value) / (2 * timespan / 1000.0f);
                posX[i].outTangent = (posX[i + 1].value - posX[i - 1].value) / (2 * timespan / 1000.0f);
                posY[i].inTangent = (posY[i + 1].value - posY[i - 1].value) / (2 * timespan / 1000.0f);
                posY[i].outTangent = (posY[i + 1].value - posY[i - 1].value) / (2 * timespan / 1000.0f);
                posZ[i].inTangent = (posZ[i + 1].value - posZ[i - 1].value) / (2 * timespan / 1000.0f);
                posZ[i].outTangent = (posZ[i + 1].value - posZ[i - 1].value) / (2 * timespan / 1000.0f);
            }

            clip.SetCurve(data, typeof(Transform), "localPosition.x", new AnimationCurve(posX));
            clip.SetCurve(data, typeof(Transform), "localPosition.y", new AnimationCurve(posY));
            clip.SetCurve(data, typeof(Transform), "localPosition.z", new AnimationCurve(posZ));

            clip.SetCurve(data, typeof(Transform), "localRotation.x", new AnimationCurve(rotX));
            clip.SetCurve(data, typeof(Transform), "localRotation.y", new AnimationCurve(rotY));
            clip.SetCurve(data, typeof(Transform), "localRotation.z", new AnimationCurve(rotZ));
            clip.SetCurve(data, typeof(Transform), "localRotation.w", new AnimationCurve(rotW));

            clip.EnsureQuaternionContinuity();

        }
    }
    
    // Use this for initialization
    public void Start()
    {
        var vrData = new[]
            {
                DataCollection<Data>.LoadData(@"Assets/Head.txt", DataType.Head),
                DataCollection<Data>.LoadData(@"Assets/Cnt2.txt", DataType.Arm),
                DataCollection<Data>.LoadData(@"Assets/Cube2.txt", DataType.Forearm),
                DataCollection<Data>.LoadData(@"Assets/Cnt1.txt", DataType.Chest),
            };

        foreach (var d in vrData) d.Rotate(_angle, axis, centerPoint);

        vrData[0].Save($@"Assets/Head_rotated.txt");
        vrData[1].Save($@"Assets/Arm_rotated.txt");
        vrData[2].Save($@"Assets/Forearm_rotated.txt");
        vrData[3].Save($@"Assets/Chest_rotated.txt");

        //return;
        Animation anim = GetComponent<Animation>();

        // create a new AnimationClip
        AnimationClip clip = new AnimationClip();
        clip.legacy = true;
        //LoadData("Head", clip);
        //LoadData("Cube2", clip);  
        //LoadData("Cnt1", clip);
        //LoadData("Cnt2", clip);
        LoadData($"Head_rotated", clip);
        LoadData($"Arm_rotated", clip);
        LoadData($"Forearm_rotated", clip);
        LoadData($"Chest_rotated", clip);

        // now animate the GameObject
        //anim.AddClip(clip, clip.name);
        //anim.Play(clip.name);

        AssetDatabase.CreateAsset(clip, $@"Assets/clip_rotated.anim");
        AssetDatabase.SaveAssets();
    }
}
