﻿using AAPD_Core.Infrastructure;
using AAPD_Core.Processors;
using AAPD_Data.Infrastructure;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class FileDataController : MonoBehaviour
{
    [SerializeField] private string _dataFilePath;
    [SerializeField] private Text _phaseText;
    [SerializeField] private Text _qualityText;

    private IDataProcessor<Data> _processor;

    // Use this for initialization
    private void Start()
    {
        _processor = new FileDataProcessor(_dataFilePath);
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        _processor.Process(out var phase, out var quality);

        _phaseText.text = $"{ _processor.GetType().Name } phase     { phase.ToString("P4", CultureInfo.InvariantCulture)}";
        _qualityText.text = $"{ _processor.GetType().Name } quality     { quality.ToString("P4", CultureInfo.InvariantCulture)}";
    }
}
